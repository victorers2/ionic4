import { Component, OnInit } from '@angular/core';
import { Post } from 'src/models/Post';
import { Usuario } from 'src/models/Usuario';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage implements OnInit {
  posts: Post[] = [];

  constructor() { }

  ngOnInit() {
    for(let i=0; i<7; i++){
      this.posts.push(new Post(
        `../assets/${i}.png`, 
        `${i+1}/12/2006`, 
        [], 
        'Título ' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10), 
        'Texto '  +Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 36), 
        new Usuario(
          'Nome ' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 36),
          Math.ceil(Math.random()*60),
          'Natal/RN',
          'Natal/RN',
          `${i+1}/09/1996`,
          '',
          '',
          '../assets/perfil.png'
        )));
      this.posts[i].upVotes = Math.ceil(Math.random()*10);
      this.posts[i].downVotes = Math.ceil(Math.random()*10);
    }

    console.log('Posts: ' , this.posts);
  }

}
