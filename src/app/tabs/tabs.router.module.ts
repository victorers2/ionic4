import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'perfil',
        children: [{ path: '', loadChildren: '../perfil/perfil.module#PerfilPageModule' }]
      },
      {
        path: 'timeline',
        children: [{ path: '', loadChildren: '../timeline/timeline.module#TimelinePageModule' }]
      },
      {
        path: 'config',
        children: [{ path: '', loadChildren: '../config/config.module#ConfigPageModule' }]
      },
      {
        path: '',
        redirectTo: '/tabs/perfil',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/perfil',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
