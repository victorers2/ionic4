import { Component, OnInit } from '@angular/core';

import { Usuario } from 'src/models/Usuario';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  public usuario: Usuario;

  constructor() { 
    this.usuario = new Usuario(
      'Victor Emanuel Ribeiro Silva', 
      22, 
      'Natal, RN', 
      'Natal, RN', 
      '13/06/1996', 
      'Solteiro', 
      'Estudante de Engenharia da Computação na UFRN',
      '../assets/perfil.png');
  }

  ngOnInit() {
  }

}
