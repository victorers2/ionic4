import { Usuario } from './Usuario';

export class Post {
    public upVotes: number = 0;
    public downVotes: number = 0;

    constructor(
        public img: string = '',
        public data: string = '',
        public comentarios: string[] = [],
        public titulo: string = '',
        public texto: string = '',
        public usuario: Usuario
    ) {
    }
}