export class Usuario {

    constructor(
        public nome: string='',
        public idade: number=0,
        public local: string='',
        public localNasc: string='',
        public dataNasc: string='',
        public estadoCivil: string='',
        public biografia: string='',
        public foto: string=''
    ) {

    }
}